alias vim="nvim"

# get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# easier to read disk
alias df='df -h'     # human-readable sizes
alias free='free -m' # show sizes in MB

# get top process eating memory
alias psmem='ps auxf | sort -nr -k 4 | head -5'

# get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3 | head -5'

# change cat for bat
alias cat='bat'

# get rid of command not found ##
alias cd..='cd ..'
 
# a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# For when keys break
alias archlinx-fix-keys="sudo pacman-key --init && sudo pacman-key --populate archlinux && sudo pacman-key --refresh-keys"

# systemd
alias mach_list_systemctl="systemctl list-unit-files --state=enabled"

#ls (replaced by eza)
# alias ls='ls --color=auto'
# alias l='ls -lFh --color=auto'
# alias la='ls -lAFh --color=auto'        
# alias lr='ls -tRFh --color=auto'        
# alias lt='ls -ltFh  --color=auto'       
# alias ll='ls -l --color=auto'      
# alias ldot='ls -ld .* --color=auto'
# alias lS='ls -1FSsh --color=auto'
# alias lart='ls -1Fcart --color=auto'
# alias lrt='ls -1Fcrt --color=auto'

# EZA
alias ld='eza -lD'
alias lf='eza -lF --color=always | grep -v /'
alias lh='eza -dl .* --group-directories-first'
alias ll='eza -al --group-directories-first'
alias lm='eza -al --sort=modified'
alias ls='eza -alF --color=always --sort=size | grep -v /'
alias lt='eza -alF --color=always --sort=size --tree | grep -v /'

## git aliases
alias ga='git add .'
alias gc='git commit -m'
alias gpush='git push -u origin main'

#Pacman cleaning
alias fullclean='$HOME/.local/bin/pacclean'

#enable full zsh history
alias history="fc -l 1"

##Calculator (bc)
alias calc="bc -l"

#Wireguard quick accessdb
alias wg0on="sudo wg-quick up wg0"
alias wg0off="sudo wg-quick down wg0 "
#Wireguard home access
alias wg2on="sudo wg-quick up wg2"
alias wg2off="sudo wg-quick down wg2"

#openVPN Danica
alias ovpndon="sudo openvpn --config /etc/openvpn/client/danica-openvpn.ovpn --daemon" 
alias ovpndoff="sudo pkill openvpn" 

##SSH Access
alias ssh-vps="ssh -p 2288 gabs@gabsalmeida.xyz"

# dotfiles
alias cdot="cd $HOME/dotfiles/"

# ping to fping
alias ping="fping -l "

## Lazy Git push function ##
lazyg(){
    git add .
    git commit -m "$1"
    git push -u origin main
}

## yazi wrapper using current directory's
function yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

##BUSSCAR gvfs files accessdb
alias departamentos="cd /run/user/1000/gvfs/smb-share:server=fileserver,share=arquivos/departamentos"
alias ti="cd /run/user/1000/gvfs/smb-share:server=fileserver,share=arquivos/departamentos/TI"
alias publico="cd /run/user/1000/gvfs/smb-share:server=fileserver,share=arquivos/publico"
