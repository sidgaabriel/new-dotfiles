return {
	"FraserLee/ScratchPad",
	config = function()
		vim.g.scratchpad_autostart = 0
		vim.g.scratchpad_autosize = 1
		vim.g.scratchpad_location = "~/.local/share/nvim/.scratchpad"
		vim.g.scratchpad_autofocus = 1
		-- Size window
		-- vim.g.scratchpad_textwidth = 50
		-- vim.g.scratchpad_minwidth = 50
		-- Set font color
		-- vim.execute("hi ScratchPad guifg=#98c379")
		vim.api.nvim_set_hl(0, "ScratchPad", { fg = "#98c379" })
		vim.keymap.set("n", "<leader>x", "<cmd>ScratchPad<cr>", {})
	end,
}
