# Fonts
font-family = "JetBrains Mono"
font-size = 10
font-style = SemiBold
font-style-bold = default
font-style-italic = default
font-style-bold-italic = default

# Theme
theme = catppuccin-mocha
gtk-adwaita = false

# Cursor
cursor-style = bar
cursor-style-blink = false

# Shell
shell-integration-features = true

# Mouse
mouse-hide-while-typing = true
mouse-scroll-multiplier = 2

# Window
window-padding-balance = true 
window-save-state = always
window-colorspace = "display-p3"
background-opacity = 0.96
initial-window = true
resize-overlay = never
window-step-resize = false
background-blur-radius = 15
confirm-close-surface = false
window-decoration = false

# keybindings
keybind = ctrl+s>r=reload_config
keybind = ctrl+s>x=close_surface
keybind = ctrl+s>n=new_window

# tabs 
keybind = ctrl+s>c=new_tab
keybind = ctrl+s>shift+l=next_tab
keybind = ctrl+s>shift+h=previous_tab
keybind = ctrl+s>comma=move_tab:-1
keybind = ctrl+s>period=move_tab:1

# quick tab switch
keybind = ctrl+s>1=goto_tab:1
keybind = ctrl+s>2=goto_tab:2
keybind = ctrl+s>3=goto_tab:3
keybind = ctrl+s>4=goto_tab:4
keybind = ctrl+s>5=goto_tab:5
keybind = ctrl+s>6=goto_tab:6
keybind = ctrl+s>7=goto_tab:7
keybind = ctrl+s>8=goto_tab:8
keybind = ctrl+s>9=goto_tab:9

# split
keybind = ctrl+s>\=new_split:right
keybind = ctrl+s>-=new_split:down
keybind = ctrl+s>j=goto_split:bottom
keybind = ctrl+s>k=goto_split:top
keybind = ctrl+s>h=goto_split:left
keybind = ctrl+s>l=goto_split:right
keybind = ctrl+s>z=toggle_split_zoom
keybind = ctrl+s>e=equalize_splits

# Clipboard
copy-on-select = clipboard
clipboard-trim-trailing-spaces = true

# Behavior
auto-update = check
auto-update-channel = stable
quit-after-last-window-closed = true
