# yaml-language-server: $schema=https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json

version: 2
final_space: true
console_title_template: "{{ .Shell }} in {{ .Folder }}"
blocks:
- type: prompt
  alignment: left
  newline: true
  segments:
  - type: path
    style: plain
    foreground: blue
    background: transparent
    template: " {{ .Path }} "
    properties:
      style: full
  - type: git
    style: plain
    foreground: p:grey
    background: transparent
    template: " {{ .HEAD }}{{ if or (.Working.Changed) (.Staging.Changed) }}*{{ end }} <cyan>{{ if gt .Behind 0 }}⇣{{ end }}{{ if gt .Ahead 0 }}⇡{{ end }}</>"
    properties:
      branch_icon: ""
      commit_icon: "@"
      fetch_status: true
- type: rprompt
  overflow: hidden
  segments:
  - type: executiontime
    style: plain
    foreground: yellow
    background: transparent
    template: "{{ .FormattedMs }} "
    properties:
      threshold: 500
- type: prompt
  alignment: left
  newline: true
  segments:
  - type: text
    style: plain
    foreground_templates: [
      "{{if gt .Code 0}}red{{end}}",
      "{{if eq .Code 0}}magenta{{end}}",
    ]
    background: transparent
    template: "❯ "
transient_prompt:
  foreground_templates: [
    "{{if gt .Code 0}}red{{end}}",
    "{{if eq .Code 0}}magenta{{end}}",
  ]
  background: transparent
  template: "❯ "
secondary_prompt:
  foreground: magenta
  background: transparent
  template: "❯❯ "
palette:
  black: "#262B44"
  blue: "#4B95E9"
  green: "#59C9A5"
  orange: "#F07623"
  red: "#D81E5B"
  white: "#E0DEF4"
  yellow: "#F3AE35"
