local function map(m, k, v, x)
	vim.keymap.set(m, k, v, x, { silent = true })
end

-- Mimic shell movements
map("i", "<C-E>", "<ESC>A")
map("i", "<C-A>", "<ESC>I")

-- Use alt + hjkl to resize windows
map("n", "<M-j>", "<cmd>resize -2<cr>")
map("n", "<M-k>", "<cmd>resize +2<cr>")
map("n", "<M-h>", "<cmd>vertical resize -2<cr>")
map("n", "<M-l>", "<cmd>vertical resize +2<cr>")

-- TAB in general mode will move to text buffer
map("n", "<TAB>", "<cmd>bnext<cr>")

-- SHIFT-TAB will go back
map("n", "<S-TAB>", "<cmd>bprevious<cr>")

-- kill active buffer
map("n", "<C-q>", "<cmd>bdelete<cr>")

-- Alternate way to save
map("n", "<C-s>", "<cmd>w<cr>")

-- CTRL-C acting as ESC key
map("n", "<C-c>", "<ESC>")

-- CTRL+D and CTRL+U centered view
map ("n", "<C-d>", "<C-d>zz")
map ("n", "<C-u>", "<C-u>zz")

-- n and N cetered search
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")

-- Better window navigation
map("n", "<C-h>", "<C-w>h")
map("n", "<C-j>", "<C-w>j")
map("n", "<C-k>", "<C-w>k")
map("n", "<C-l>", "<C-w>l")

-- Leader key insert new blank line bellow/above
map("n", "<Leader>o", "o<Esc>")
map("n", "<Leader>O", "O<Esc>")

-- Markdown preview
map("n", "<leader>mp", "<cmd>MarkdownPreview<cr>")
map("n", "<leader>ps", "<cmd>MarkdownPreviewStop<cr>")
map("n", "<leader>pt", "<cmd>MarkdownPreviewToggle<cr>")

-- Undotree
map("n", "<leader>u", "<cmd>UndotreeToggle<cr>")

-- Fugitive
map("n", "<leader>gs", "<cmd>Git<cr>")

-- Move highlighted itens
map("v", "J", ":m '>+1<cr>gv=gv")
map("v", "K", ":m '<-2<cr>gv=gv")

-- Mantain cursor at J append 
map("n", "J", "mzJ`z")

-- paste over highlighted text without loosing buffer
map("x", "<leader>p", "\"_dP")

-- replacement string helper
map("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- next greatest remap ever : asbjornHaland
map({"n", "v"}, "<leader>y", [["+y]])
map("n", "<leader>Y", [["+Y]])

-- Create new file
map("n", "<leader>n", "<cmd>ene!<cr>")

-- Close tab using leader + c
map("n", "<leader>c", "<cmd>bd<cr>")

-- Open new split
map("n", "<leader>v", "<cmd>vsplit<cr>")
