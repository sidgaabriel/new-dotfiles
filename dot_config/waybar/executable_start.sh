#!/bin/sh

CONFIG="$HOME/.config/waybar/config"
STYLES="$HOME/.config/waybar/style.css"

killall -q waybar

waybar --config ${CONFIG} --style ${STYLES} 2>&1 | tee -a /tmp/waybar.log & disown

echo "Waybar launched"

