return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	config = function()
		local config = require("nvim-treesitter.configs")
		config.setup({
			ensure_installed = { "bash", "lua", "markdown", "vim", "vimdoc", "dockerfile" },
			auto_install = true,
			sync_install = false,
			highlight = { enable = true },
			indent = { enable = true },
			additional_vim_regex_highlighting = { "markdown" },
		})
	end,
}
