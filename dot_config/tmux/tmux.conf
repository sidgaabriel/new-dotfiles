# Set true color
set-option -sa terminal-overrides ",xterm*:Tc"

# Enable mouse support
set -g mouse on

# Start windows and panes at 1, not 0
set -g base-index 1
set -g pane-base-index 1
set-window-option -g pane-base-index 1
set-option -g renumber-windows on

# Remap prefix key
unbind C-b
set -g prefix C-Space
bind C-Space send-prefix

# set vi-mode
#set-window-option -g mode-keys vi
bind 'v' copy-mode

#Keybindings
bind -n M-h previous-window
bind -n M-l next-window

bind r source-file ~/.config/tmux/tmux.conf \; display-message "tmux.conf reloaded"

bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-selection
bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

# Open panes in current directory
bind '"' split-window -v -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'tmux-plugins/tmux-yank'

# Colorscheme
set -g @plugin 'catppuccin/tmux'
set -g @catppuccin_flavour 'mocha'

# window icons
set -g @catppuccin_icon_window_last "󰖰 "
set -g @catppuccin_icon_window_current "󰖯 "
set -g @catppuccin_icon_window_zoom "󰁌 "
set -g @catppuccin_icon_window_mark "󰃀 "
set -g @catppuccin_icon_window_silent "󰂛 "
set -g @catppuccin_icon_window_activity "󱅫 "
set -g @catppuccin_icon_window_bell "󰂞 "

set -g @catppuccin_window_left_separator ""
set -g @catppuccin_window_right_separator " "
set -g @catppuccin_window_middle_separator " █"
set -g @catppuccin_window_number_position "right"

# window default format
set -g @catppuccin_window_default_fill "number"
set -g @catppuccin_window_current_text "#W"

# window current format
set -g @catppuccin_window_current_fill "number"
set -g @catppuccin_window_current_text "#W"

#Status module list
set -g @catppuccin_status_modules_right "application directory session"
set -g @catppuccin_status_left_separator  " "
set -g @catppuccin_status_right_separator ""
set -g @catppuccin_status_fill "icon"
set -g @catppuccin_status_connect_separator "no"

set -g @catppuccin_directory_text $cwd

# bootstrap tpm
if "test ! -d ~/.tmux/plugins/tpm" \
   "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins'"

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
