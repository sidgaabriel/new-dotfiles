local g = vim.g
local o = vim.o
local opt = vim.opt

-- Map <leader> to space
g.mapleader = " "
g.maplocalleader = " "

-- Main configs
o.hidden = true
o.wrap = false
o.encoding = "utf-8"
o.fileencoding = "utf-8"
o.pumheight = 10
o.ruler = true
-- o.cmdheight = 2
o.splitbelow = true
o.splitright = true
o.tabstop = 2
o.shiftwidth = 4
o.softtabstop = 4
o.smarttab = true
o.expandtab = true
o.smartindent = true
o.autoindent = true
o.cindent = true
o.number = true
o.relativenumber = true
o.numberwidth = 2
o.cursorline = true
o.scrolloff = 8
o.cursorline = true
-- o.showtabline = 2
o.showmode = false
o.incsearch = true
o.inccommand = "split"
o.hlsearch = false
o.backup = false
o.writebackup = false
o.undodir = os.getenv("HOME") .. "/.vim/undodir"
o.undofile = true
o.swapfile = false
o.updatetime = 50
o.clipboard = "unnamedplus"
o.history = 50
o.termguicolors = true
--o.colorcolumn = "120"
opt.mouse = "a"
